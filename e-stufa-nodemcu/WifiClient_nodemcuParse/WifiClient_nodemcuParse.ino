#include <e-stufaWifiManager.h>

#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <user_interface.h>
#include <DHT.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <virtuabotixRTC.h>

//definindo o número de bytes da memória do EEPROM que vamos usar (como se fosse o tamanho do array, onde cada casa é um byte)
#define MEM_ALOC_SIZE 8
#define DHT_DATA_PIN 13 // pino que estamos conectando
#define DHTTYPE DHT11

DHT dht(DHT_DATA_PIN, DHTTYPE);

// Determina os pinos ligados ao modulo
// myRTC(clock, data, rst)
virtuabotixRTC myRTC(12, 15, 14);

const char* host     = "estufa.herokuapp.com"; // Your domain 

//URL e-stufa esquilo:
//String url = "/api/dev/58014e9dacae491f007c00e7";

//URL e-stufa rique:
//String url = "/api/dev/57f6b3d36efde11f00531065";

//URL e-stufa TESTE:
String url = "/api/dev/58405fc89f99732000c6caac";

//URL do lightstatus. Tem que ajustar
//String urlLightStatus = "/api/dev/light/58014e9dacae491f007c00e7/";
//String urlLightStatus = "/api/dev/light/57f6b3d36efde11f00531065/";
String urlLightStatus = "/api/dev/light/58405fc89f99732000c6caac/";

//const char* deviceId   = "57f6b3d36efde11f00531065";
//url += deviceId;

//variáveis do timer da luz
int horaT = -1;
int minutoT = -1;
int segundoT = -1;

int horaD = -1;
int minutoD = -1;
int segundoD = -1;
//fim variáveis timer da luz

const int relayWater        = 5;
const int relayLight        = 16;
const int relayCooler        = 4;

const int resetWifi        = 2;

uint8_t temperature;
uint8_t waterTimer;
uint8_t lightTimer;
uint8_t lightDuration;
bool lightStatus = false;

uint8_t localTemperature;
uint8_t localWaterTimer;
uint8_t localLightTimer = 0;
uint8_t localLightDuration = 0;
bool localLightStatus = false;

bool _timeout = false;
bool _timeoutDuration = false;
// Descomentar para Timer de Agua
//bool _timeoutAgua = false;
//bool _req = false;
bool _lightChange = false;
bool _lightUpdate = false;

void setup() {  

  pinMode(resetWifi, INPUT);
  
  pinMode(relayLight, OUTPUT); 
  digitalWrite(relayLight, LOW);
  pinMode(relayCooler, OUTPUT); 
  digitalWrite(relayCooler, LOW);
  pinMode(relayWater, OUTPUT); 
//  Como não estamos usando bomba, esse relé sempre ficará low
  digitalWrite(relayWater, LOW);
  Serial.begin(9600);
  
  //iniciando a memória não-volátil local
  EEPROM.begin(MEM_ALOC_SIZE);
  
  delay(10);
  
    //WiFiManager
    //Local intialization. Once its business is done, there is no need to keep it around
   WiFiManager wifiManager;
    //reset saved settings
//    wifiManager.resetSettings();
    
    //set custom ip for portal
    //wifiManager.setAPConfig(IPAddress(10,0,1,1), IPAddress(10,0,1,1), IPAddress(255,255,255,0));

    //fetches ssid and pass from eeprom and tries to connect
    //if it does not connect it starts an access point with the specified name
    //here  "AutoConnectAP"
    //and goes into a blocking loop awaiting configuration
    wifiManager.autoConnect("e-stufaWifiTeste");
    //or use this for auto generated name ESP + ChipID
    //wifiManager.autoConnect();

    
    //if you get here you have connected to the WiFi
    Serial.println("Conectado no wifi :)");

    
//    --conexão wifi hardcoded
//  Serial.print("Connecting to ");
//  Serial.println(ssid);
//
//  WiFi.begin(ssid, password);
//  int wifi_ctr = 0;
//  while (WiFi.status() != WL_CONNECTED) {
//    delay(500);
//    Serial.print(".");
//  }
//
//  Serial.println("WiFi connected");  
////  o comando abaixo deu pau da última vez, então, eu comentei
//  Serial.println("IP address: ");
//  Serial.println(WiFi.localIP());
  myRTC.setDS1302Time(00, 58, 23, 2, 17, 11, 2014);
  dht.begin();
}

void loop() {  

  //Lê a informação do pino de resetWifi pra ver se é necessário resetar wifi
  if(digitalRead(resetWifi) == LOW){
    Serial.print("\n\n\nRESETAR WIFI\n\n\n");
    WiFiManager wifiManager;
    wifiManager.resetSettings();
    wifiManager.autoConnect("e-stufaWifiTeste");
  }

  // Le as informacoes do CI
  myRTC.updateTime(); 

  //Ajustando o timer no momento da inicialização
  if(horaT == -1){
    if(localLightDuration>0){
      Serial.println("Ligar Luz primeira vez!");
      digitalWrite(relayLight, HIGH);
      _lightChange = true;
      localLightStatus = true;
      //ligamos a luz e setamos o timer para calcular a duração da luz
      setTimer1(localLightDuration);
    }
   }

  //checagem recorrente das variáveis de timer.
 if(!localLightStatus){
    if(horaD == myRTC.hours &&  minutoD <= myRTC.minutes && segundoD <= myRTC.seconds){
      Serial.print("\n\n\nLIGAR LUZ!!!\n\n\n");  
      digitalWrite(relayLight, HIGH);
      _lightChange = true;
      localLightStatus = true;
      setTimer1(localLightDuration);
    }
  } else {
      if(horaD == myRTC.hours && minutoD <= myRTC.minutes && segundoD <= myRTC.seconds){
        Serial.print("\n\n\nDESLIGAR LUZ!!!\n\n\n"); 
        digitalWrite(relayLight, LOW); 
        _lightChange = true;
        localLightStatus = false;
        setTimer1(localLightTimer);
      }
  }
  
  
  Serial.print("connecting to ");
  Serial.println(host);
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
//    return;
  }

  float liveHumidity = dht.readHumidity();
  float liveTemperature = dht.readTemperature();


  if(liveTemperature > localTemperature){
      Serial.println("ligar cooler");
      digitalWrite(relayCooler, HIGH);
    } else {
      Serial.println("desligar cooler");
      digitalWrite(relayCooler, LOW);
    }
  

  if(_lightChange){

    client.print(String("POST ") + urlLightStatus + (String) localLightStatus + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: keep-alive\r\n\r\n");

    _lightChange = false;
    _lightUpdate = true;
    
    }

  client.print(String("POST ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: keep-alive\r\n\r\n");

  delay(500); // wait for server to respond

  // antes de ler o servidor, recupera as informações da memória local e compara para salvar ou não
  localTemperature = EEPROM.read(0);
  localWaterTimer = EEPROM.read(1);
  localLightDuration = EEPROM.read(2);
  localLightTimer = EEPROM.read(3);
  

  // lendo resposta do servidor caso tenha conseguido se conectar
  String section="header";
  while(client.available()){
    String line = client.readStringUntil('\r');
    // Serial.print(line);
    // we’ll parse the HTML body here
    if (section=="header") { // headers..
      Serial.print(".");
      if (line=="\n") { // skips the empty space at the beginning 
        section="json";
      }
    }
    else if (section=="json") {  // print the good stuff
      section="ignore";
      String result = line.substring(1);

      // Parse JSON
      int size = result.length() + 1;
      char json[size];
      result.toCharArray(json, size);
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& json_parsed = jsonBuffer.parseObject(json);
      if (!json_parsed.success())
      {
        Serial.println("parseObject() failed");
//        return;
      }
      

      if (json_parsed["temperature"]) {
        temperature = (int)json_parsed["temperature"];
        if(temperature != localTemperature){
           localTemperature = temperature;
           EEPROM.write(0, temperature);
          }
        waterTimer = (int)json_parsed["waterTimer"];
        if(waterTimer != localWaterTimer){
          localWaterTimer = waterTimer;
          EEPROM.write(1, waterTimer);
        }
        lightDuration = (int)json_parsed["lightDuration"];
        if(lightDuration != localLightDuration){
          localLightDuration = lightDuration;
          EEPROM.write(2, lightDuration);
        }
        lightTimer = (int)json_parsed["lightTimer"];
        if(lightTimer != localLightTimer){
          localLightTimer = lightTimer;
          EEPROM.write(3, lightTimer);
        }  

        //salvando de forma concreta qualquer alteração que tenha sido feita na memória local
        EEPROM.commit();


        Serial.println("\n LightStatus:");
        Serial.println((int)json_parsed["lightStatus"]);
        if( (int)json_parsed["lightStatus"] > 0 ){
          
          lightStatus = true;
        } else {
          lightStatus = false;
        }
        
        if(!_lightUpdate){

//          Serial.println("LGHT UPDATE");
//          Serial.println(lightStatus);
//          Serial.println(localLightStatus);
          if(lightStatus != localLightStatus){
           
            if(lightStatus){
              Serial.println("Ligar LUZ SERVIDOR");
              localLightStatus = lightStatus;
              digitalWrite(relayLight, HIGH);
              setTimer1(localLightDuration);

            } else {
              
              Serial.println("desligar luz");
              digitalWrite(relayLight,LOW);
              localLightStatus = lightStatus;  
              setTimer1(localLightTimer);
   
            }
            
          }
         }
        _lightUpdate = false;
      }
      else {
        Serial.println("NADA FEITO");
      }
    }
  }

  Serial.println();
  Serial.print("Temperatura máxima: ");
  Serial.println(localTemperature);
  Serial.print("Timer da luz: ");
  Serial.println(localLightTimer);
  Serial.print("Duração da luz: ");
  Serial.println(localLightDuration);
  Serial.print("Timer da água: ");
  Serial.println(localWaterTimer);
  Serial.print("Temperatura atual: ");
  Serial.println(liveTemperature);
  Serial.print("Luz Status: ");
  Serial.println(localLightStatus);
  Serial.print("closing connection. ");
}

//função que altera os valores das variáveis de timer que serão checadas recorrentemente no início do código
void setTimer1(int duration)
{
    horaT = myRTC.hours;
    minutoT = myRTC.minutes;
    segundoT = myRTC.seconds;  

//    if(segundoT + duration > 60){
//      segundoD = segundoT + duration - 60;
//      if(minutoT == 59){
//          minutoD = 0;
//          horaD = horaT+1;
//
//          if(horaD == 24){
//            horaD = 0;
//          }
//      } else {
//        minutoD = minutoT + 1;
//        horaD = horaT;
//      }
//     } else {
//      segundoD = segundoT + duration;
//      minutoD = minutoT;
//      horaD = horaT;
//     }    

  if(horaT + duration >= 24){
    horaD = horaT + duration - 24;  
  } else {
    horaD = horaT + duration;
  }

  minutoD = minutoT;
  segundoD = segundoT;
  
}

