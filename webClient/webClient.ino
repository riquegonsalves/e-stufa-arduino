// Demo using DHCP and DNS to perform a web client request.
// 2011-06-08 <jc@wippler.nl> http://opensource.org/licenses/mit-license.php
#include <ArduinoJson.h>
#include <EtherCard.h>

// ethernet interface mac address, must be unique on the LAN
static byte mymac[] = { 0x74,0x69,0x69,0x2D,0x30,0x31 };

byte Ethernet::buffer[800];
static uint32_t timerRequest;
static uint32_t timerLight = 0;
static uint32_t timerLightDuration = 0;
static uint32_t timerWater = 0;
//static uint32_t timerWaterDuration = 0;

int temperature;
int water;
int light;
int lightDuration;
// Duraçao da bomba de agua ligada em milisegundos (2000 = 2s)
//int waterDuration = 2000;

// cooler recebe se o cooler esta ligado ou desligado
int cooler = 0;

// temperatura que recebe o sensor  
int liveTemp = 25;

//const char website[] PROGMEM = "192.168.1.101";
const char website[] PROGMEM = "estufa.herokuapp.com";



// called when the client request is complete
static void my_callback (byte status, word off, word len) {
  Serial.println(">>>");
  Ethernet::buffer[off+800] = 0;
  StaticJsonBuffer<300> jsonBuffer;
  char * data = (char *) Ethernet::buffer + off;
  char * temp = (char *) Ethernet::buffer + off;
// producao  
  temp+=234; 
// local  
// temp+=200; 
//  
  JsonObject& obj = jsonBuffer.parseObject(temp);
    
   temperature = obj["device"]["temperature"];
   water = obj["device"]["waterTimer"];
   light = obj["device"]["lightTimer"];
   lightDuration = obj["device"]["lightDuration"];
    
   if(timerLight == 0 || timerWater == 0){ 
     timerLight = (uint32_t) light;
     timerWater = (uint32_t) water;
   }
   
   
  
  if(liveTemp > temperature){
    if(cooler == 0){
      cooler = 1;
      Serial.print("\n Ligar Cooler");
    }  
  } else {
    if(cooler == 1 && liveTemp < temperature-1){
      Serial.print("\n Desligar Cooler");
      cooler = 0;
    }
  }
//  Debug de array que esta recebendo
//  obj.printTo(Serial);
  Serial.print("\n Temperatura Maxima: ");
  Serial.print(temperature);
   Serial.print("\n Agua Timer: ");
  Serial.print(water);
   Serial.print("\n Luz Timer: ");
  Serial.print(light);
   Serial.print("\n Luz Duracao: ");
  Serial.print(lightDuration);
  Serial.print("\n");
  Serial.print(temp);
  Serial.print("\n");
}


void setup () {
  Serial.begin(57600);
  Serial.println(F("\n[webClient]"));

  if (ether.begin(sizeof Ethernet::buffer, mymac, 8) == 0) 
    Serial.println(F("Failed to access Ethernet controller"));
  if (!ether.dhcpSetup())
    Serial.println(F("DHCP failed"));

  ether.printIp("IP:  ", ether.myip);
  ether.printIp("GW:  ", ether.gwip);  
  ether.printIp("DNS: ", ether.dnsip);  

  if (!ether.dnsLookup(website))
    Serial.println("DNS failed");
//  ether.hisip[0] = 192;
//  ether.hisip[1] = 168;
//  ether.hisip[2] = 1;
//  ether.hisip[3] = 101;
//  ether.hisport = 3000;  
  ether.printIp("SRV: ", ether.hisip);
  
}

void loop () {
  ether.packetLoop(ether.packetReceive());
  
  if(timerLightDuration > 0 && millis() > timerLightDuration){
    timerLightDuration = 0;
    Serial.print("\nDESLIGAR LUZ");
  }
 
  if (timerLight > 0 && millis() > timerLight) {
    
    unsigned long lightTime = (light + lightDuration) * 60000;
    timerLight = millis() + lightTime;
    
    if(lightDuration > 0){
      unsigned long lightDurationTime = lightDuration * 60000;
      timerLightDuration = millis() + lightDurationTime;
    }
    
    Serial.println();
    Serial.print("\nLIGAR LUZ"); 
  }
    
 
  if(timerWater > 0 && millis() > timerWater){
    unsigned long waterTime = water * 60000;
    timerWater = millis() + waterTime;
    Serial.print("\nLIGAR AGUA");
  }
  
  
  if (millis() > timerRequest) {
    timerRequest = millis() + 5000;
    Serial.println();
    Serial.print("<<< REQ ");
    //local
//    ether.httpPost(PSTR("/api/dev/57f57d80a51e7c3896a5f2a5"), website, NULL,"content data", my_callback);
    //producao
    ether.httpPost(PSTR("/api/dev/57f6b3d36efde11f00531065"), website, NULL,"content data", my_callback);
    
 
  }
}
